package com.jogym.common.exception;

public class CExpireSeasonTicketDateEndException extends RuntimeException {
    public CExpireSeasonTicketDateEndException(String msg, Throwable t) {
        super(msg, t);
    }

    public CExpireSeasonTicketDateEndException(String msg) {
        super(msg);
    }

    public CExpireSeasonTicketDateEndException() {
        super();
    }
}
