package com.jogym.common.exception;

public class CDuplicateBusinessNumberException extends RuntimeException{
    public CDuplicateBusinessNumberException(String msg, Throwable t) {
        super(msg, t);
    }

    public CDuplicateBusinessNumberException(String msg) {
        super(msg);
    }

    public CDuplicateBusinessNumberException() {
        super();
    }
}