package com.jogym.common.exception;

public class CExistIngCalculateHistoryException extends RuntimeException{
    public CExistIngCalculateHistoryException(String msg, Throwable t) {
        super(msg, t);
    }

    public CExistIngCalculateHistoryException(String msg) {
        super(msg);
    }

    public CExistIngCalculateHistoryException() {
        super();
    }
}