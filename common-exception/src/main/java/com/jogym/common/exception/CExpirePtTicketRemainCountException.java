package com.jogym.common.exception;

public class CExpirePtTicketRemainCountException extends RuntimeException {
    public CExpirePtTicketRemainCountException(String msg, Throwable t) {
        super(msg, t);
    }

    public CExpirePtTicketRemainCountException(String msg) {
        super(msg);
    }

    public CExpirePtTicketRemainCountException() {
        super();
    }
}
