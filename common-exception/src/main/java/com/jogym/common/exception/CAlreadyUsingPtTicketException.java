package com.jogym.common.exception;

public class CAlreadyUsingPtTicketException extends RuntimeException {
    public CAlreadyUsingPtTicketException(String msg, Throwable t) {
        super(msg, t);
    }

    public CAlreadyUsingPtTicketException(String msg) {
        super(msg);
    }

    public CAlreadyUsingPtTicketException() {
        super();
    }
}
