package com.jogym.api.goods.service;

import com.jogym.api.goods.entity.Trainer;
import com.jogym.api.goods.repository.TrainerRepository;
import com.jogym.common.exception.CMissingDataException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TrainerService {
    private final TrainerRepository trainerRepository;

    public Trainer getOriginTrainer(long trainerId) {
        return trainerRepository.findById(trainerId).orElseThrow(CMissingDataException::new);
    }

}
