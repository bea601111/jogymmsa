package com.jogym.api.goods.model.seasonticket;

import com.jogym.common.enums.TicketType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class SeasonTicketRequest {

    @NotNull
    @Length(min = 2, max = 20)
    @ApiModelProperty(notes = "정기권명(2~20)", required = true)
    private String ticketName;

    @NotNull
    @Min(0)
    @ApiModelProperty(notes = " 최대 월", required = true)
    private Integer maxMonth;

    @NotNull
    @ApiModelProperty(notes = "요금", required = true)
    private Double unitPrice;
}
