package com.jogym.api.goods.service;

import com.jogym.api.goods.entity.PtTicket;
import com.jogym.api.goods.entity.StoreMember;
import com.jogym.api.goods.entity.Trainer;
import com.jogym.common.exception.CMissingDataException;
import com.jogym.common.response.model.ListResult;
import com.jogym.api.goods.model.ptticket.PtTicketItem;
import com.jogym.api.goods.model.ptticket.PtTicketRequest;
import com.jogym.api.goods.model.ptticket.PtTicketUpdateRequest;
import com.jogym.api.goods.repository.PtTicketRepository;
import com.jogym.common.response.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PtTicketService {
    private final PtTicketRepository ptTicketRepository;

    public void setPtTicket(StoreMember storeMember, Trainer trainer, PtTicketRequest request) {
        // Builder에서 사용했던 자료들을 이곳에 담아 기능 수행을 위해 준비하는 과정
        if (!trainer.getStoreMember().equals(storeMember)) throw new CMissingDataException();

        PtTicket ptTicket = new PtTicket.PtTicketBuilder(storeMember, trainer, request).build();
        // PtTicket안에 있는 Builder를 사용하는 과정, Builder를 사용하기 위해서 필요한 자료들을 준비한 자리에서 가져와 사용한다.

        ptTicketRepository.save(ptTicket);
        // Repository 에게 원본 자료들을 저장해달라고 요청하는 과정
    }

    public ListResult<PtTicketItem> getPtTicket(StoreMember storeMember, int page) { // 사용할 값인 page의 타입을 알려줌
        Page<PtTicket> originList = ptTicketRepository.findAllByStoreMemberAndIsEnabledOrderByDateCreate(storeMember, true, ListConvertService.getPageable(page));
        // Page처리할 내용이 무엇인 지 <> 안에 담고 originList로 이름을 정의하며 repository에 작성해둔 명렁어를 사용해 id값과 Page의 총 수를 기입한다.
        List<PtTicketItem> result = new LinkedList<>();
        // 내용들을 담을 굴비줄을 하나 생성한다.

        for (PtTicket ptTicket : originList.getContent()) { // Builder에 들어간 내용들을 반복시키는 과정.
            result.add(new PtTicketItem.Builder(ptTicket).build());
        }

        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
        // page를 만드는 데에 있어 필요한 굴비줄과 총 개수, 총 페이지 수, 페이지의 숫자를 기입한다.
    }

    public void putPtTicet(long ptTicketId, PtTicketUpdateRequest request) {
        // 기능을 수행하기 위해 필요한 자료들을 담아 준비하는 과정

        PtTicket ptTicket = ptTicketRepository.findById(ptTicketId).orElseThrow(CMissingDataException::new);
        // 여기서 필요한 PtTicketId는 원본데이터이기때문에 ptTicketRepository을 호출해 PtTicketId를 가져와달라고 요청하고 없으면 던져달라고 다시 요청하는 과정

        if(!ptTicket.getIsEnabled()) throw new CMissingDataException();     // 삭제된 PT권 입니다.

        ptTicket.putPtTicket(request);
        // PtTicket Entity안에 정의된 putPtTicket을 불러 Builder를 가져온다. Builder에 사용된 Request도 함께 작성

        ptTicketRepository.save(ptTicket); // Repository한테 저장해달라고 요청
    }

    // PT 권 isEnable = false 변경하여 저장
    public void putPtTicketDelete(long ptTicketId) {
        PtTicket ptTicket = ptTicketRepository.findById(ptTicketId).orElseThrow(CMissingDataException::new);
        if(!ptTicket.getIsEnabled()) throw new CMissingDataException();     // 삭제된 PT권 입니다.
        ptTicket.delPtTicket();
        ptTicketRepository.save(ptTicket);

    }
    // 특정 자료만 삭제(isEnable = false) 위해 long 타입으로 id를 받고 repository 에게 이야기한 id의 삭제(isEnable = false) 후 저장해달라고 요청
}
