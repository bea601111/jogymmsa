package com.jogym.api.goods.entity;

import com.jogym.common.enums.TicketType;
import com.jogym.common.interfaces.CommonModelBuilder;
import com.jogym.api.goods.model.seasonticket.SeasonTicketRequest;
import com.jogym.api.goods.model.seasonticket.SeasonTicketUpdateRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class SeasonTicket {
    // 시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 회원등록일
    @Column(nullable = false)
    private LocalDateTime dateCreate;

    // 가맹점id
    @JoinColumn(name = "storeMemberId", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private StoreMember storeMember;

    // 구분 / 일 or 월
    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 10)
    private TicketType ticketType;

    // 정기권명
    @Column(nullable = false, length = 20)
    private String ticketName;

    // 최대월
    @Column(nullable = false)
    private Integer maxMonth;

    // 요금
    @Column(nullable = false)
    private Double unitPrice;

    // 사용유무
    @Column(nullable = false)
    private Boolean isEnabled;

    public void putSeasonTicketType(TicketType ticketType){
        this.ticketType = ticketType;
    }

    public void putSeasonTicket(SeasonTicketUpdateRequest request) {
        this.ticketName = request.getTicketName();
        this.maxMonth = request.getMaxMonth();
        this.unitPrice = request.getUnitPrice();
    }

    private SeasonTicket(SeasonTicketBuilder builder) {
        this.dateCreate = builder.dateCreate;
        this.storeMember = builder.storeMember;
        this.ticketType = builder.ticketType;
        this.ticketName = builder.ticketName;
        this.maxMonth = builder.maxMonth;
        this.unitPrice = builder.unitPrice;
        this.isEnabled = builder.isEnabled;
    }

    public void putSeasonTicketDelete() {
        this.isEnabled = false;
    }

    public static class SeasonTicketBuilder implements CommonModelBuilder<SeasonTicket> {

        private final LocalDateTime dateCreate;
        private final StoreMember storeMember;
        private final TicketType ticketType;
        private final String ticketName;
        private final Integer maxMonth;
        private final Double unitPrice;
        private final Boolean isEnabled;

        public SeasonTicketBuilder(StoreMember storeMember, SeasonTicketRequest request) {
            this.dateCreate = LocalDateTime.now();
            this.storeMember = storeMember;
            this.ticketType = request.getMaxMonth() > 0 ? TicketType.MONTH : TicketType.DAY;
            this.ticketName = request.getTicketName();
            this.maxMonth = request.getMaxMonth();
            this.unitPrice = request.getUnitPrice();
            this.isEnabled = true;
        }


        @Override
        public SeasonTicket build() {
            return new SeasonTicket(this);
        }
    }

}
