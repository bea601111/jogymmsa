package com.jogym.api.goods.entity;

import com.jogym.common.enums.MemberGroup;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class StoreMember {
    //시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 회원가입일
    @Column(nullable = false)
    private LocalDateTime dateCreate;

    // 맴버그룹
    @Column(nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    private MemberGroup memberGroup;

    // 아이디
    @Column(nullable = false, unique = true, length = 30)
    private String username;

    // 비밀번호
    @Column(nullable = false)
    private String password;

    // 이름
    @Column(nullable = false, length = 20)
    private String name;

    // 연락처
    @Column(nullable = false, length = 13, unique = true)
    private String phoneNumber;

    // 사업자 등록번호
    @Column(nullable = false, length = 12, unique = true)
    private String businessNumber;

    // 가맹점명
    @Column(nullable = false, length = 50)
    private String storeName;

    // 주소
    @Column(nullable = false, length = 100)
    private String address;

    // 사용유무
    @Column(nullable = false)
    private Boolean isEnabled;


}
