package com.jogym.api.calculate.repository;

import com.jogym.api.calculate.entity.SeasonTicketBuyHistory;
import com.jogym.api.calculate.entity.StoreMember;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface SeasonTicketBuyHistoryRepository extends JpaRepository<SeasonTicketBuyHistory, Long> {
    List<SeasonTicketBuyHistory> findAllByCustomer_StoreMemberAndDateCreateBetween(StoreMember storeMember, LocalDateTime startDate, LocalDateTime endDate);
}
