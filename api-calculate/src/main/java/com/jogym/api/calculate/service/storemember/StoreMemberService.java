package com.jogym.api.calculate.service.storemember;

import com.jogym.api.calculate.entity.StoreMember;
import com.jogym.common.exception.CMissingDataException;
import com.jogym.api.calculate.repository.StoreMemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class StoreMemberService {
    private final StoreMemberRepository storeMemberRepository;

    public StoreMember setStore(long id) {
        return storeMemberRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

}
