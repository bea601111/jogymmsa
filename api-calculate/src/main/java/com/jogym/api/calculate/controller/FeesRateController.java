package com.jogym.api.calculate.controller;

import com.jogym.api.calculate.model.FeesRate.FeesRateItem;
import com.jogym.api.calculate.model.FeesRate.FeesRateRequest;
import com.jogym.api.calculate.service.feeRates.FeesRateService;
import com.jogym.common.response.model.CommonResult;
import com.jogym.common.response.model.ListResult;
import com.jogym.common.response.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "수수료율 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/fees-rate")
public class FeesRateController {
    private final FeesRateService feesRateService;

    @ApiOperation(value = "수수료율 등록 // 관리자") // 관리자
    @PostMapping("/data")
    public CommonResult setFeesRate(@RequestBody @Valid FeesRateRequest request) {
        feesRateService.setFeesRate(request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "수수료율 리스트 // 관리자") // 관리자
    @GetMapping("/list")
    public ListResult<FeesRateItem> getFeesRate() {
        return ResponseService.getListResult(feesRateService.getFeesRate(), true);
    }

    @ApiOperation(value = "수수료율 수정 // 관리자") // 관리자
    @PutMapping("/fees-rate-id/{feesRateId}")
    public CommonResult putFeesRate(@PathVariable long feesRateId, @RequestBody @Valid FeesRateRequest request) {
        feesRateService.putFeesRate(feesRateId, request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "수수료율 삭제 // 관리자") // 관리자
    @DeleteMapping("/fees-rate-id/{feesRateId}")
    public CommonResult delFeesRate(@PathVariable long feesRateId) {
        feesRateService.delFeesRate(feesRateId);

        return ResponseService.getSuccessResult();
    }
}
