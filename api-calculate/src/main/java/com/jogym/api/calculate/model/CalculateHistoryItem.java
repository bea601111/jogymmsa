package com.jogym.api.calculate.model;

import com.jogym.api.calculate.entity.CalculateHistory;
import com.jogym.common.enums.CalculateStatus;
import com.jogym.common.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CalculateHistoryItem {

    private Long id;
    private Integer dateCreateYear;
    private Integer dateCreateMonth;
    private CalculateStatus calculateStatus;

    private CalculateHistoryItem(Builder builder){
        this.id = builder.id;
        this.dateCreateYear = builder.dateCreateYear;
        this.dateCreateMonth = builder.dateCreateMonth;
        this.calculateStatus = builder.calculateStatus;
    }
    public static class Builder implements CommonModelBuilder<CalculateHistoryItem>{

        private final Long id;
        private final Integer dateCreateYear;
        private final Integer dateCreateMonth;
        private final CalculateStatus calculateStatus;

        public Builder(CalculateHistory calculateHistory){
            this.id = calculateHistory.getId();
            this.dateCreateYear = calculateHistory.getDateCreateYear();
            this.dateCreateMonth = calculateHistory.getDateCreateMonth();
            this.calculateStatus = calculateHistory.getCalculateStatus();
        }

        @Override
        public CalculateHistoryItem build() {
            return new CalculateHistoryItem(this);
        }
    }
}
