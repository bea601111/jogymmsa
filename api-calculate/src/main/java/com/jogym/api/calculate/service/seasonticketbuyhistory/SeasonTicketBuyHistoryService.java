package com.jogym.api.calculate.service.seasonticketbuyhistory;

import com.jogym.api.calculate.entity.SeasonTicketBuyHistory;
import com.jogym.api.calculate.entity.StoreMember;
import com.jogym.api.calculate.repository.SeasonTicketBuyHistoryRepository;
import com.jogym.common.function.CommonDate;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SeasonTicketBuyHistoryService {
    private final SeasonTicketBuyHistoryRepository seasonTicketBuyHistoryRepository;

    public Double totalPriceSeasonTicketByYearMonth(StoreMember storeMember, int year, int month){
        LocalDateTime startDateTime = LocalDateTime.of(year, month, 1,0,0,0);
        LocalDateTime endDateTime = LocalDateTime.of(year, month, CommonDate.getLastDay(year, month),23,59,59);
        double totalPrice = 0D;
        List<SeasonTicketBuyHistory> originList = seasonTicketBuyHistoryRepository.findAllByCustomer_StoreMemberAndDateCreateBetween(storeMember, startDateTime, endDateTime);
        for (SeasonTicketBuyHistory item : originList) {
            totalPrice += (item.getSeasonTicket().getUnitPrice() * item.getSeasonTicket().getMaxMonth());
        }
        return totalPrice;
    }
}

