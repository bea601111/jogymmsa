package com.jogym.api.calculate.entity;

import com.jogym.common.enums.BuyStatus;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class SeasonTicketBuyHistory {
    // 시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 등록일시
    @Column(nullable = false)
    private LocalDateTime dateCreate;

    // 회원 id
    @JoinColumn(name = "customerId", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Customer customer;

    // 정기권 id
    @JoinColumn(name = "seasonTicketId", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private SeasonTicket seasonTicket;

     // 시작일
    @Column(nullable = false)
    private LocalDate dateStart;

    // 종료일
    @Column(nullable = false)
    private LocalDate dateEnd;

    // 최근방문일
    private LocalDate dateLast;

    // 상태
    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 10)
    private BuyStatus buyStatus;

}
