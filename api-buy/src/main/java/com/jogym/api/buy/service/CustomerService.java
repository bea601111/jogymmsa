package com.jogym.api.buy.service;

import com.jogym.api.buy.entity.Customer;
import com.jogym.api.buy.repository.CustomerRepository;
import com.jogym.common.exception.CMissingDataException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CustomerService {
    private final CustomerRepository customerRepository;

    public Customer getData(long customerId) {
        return customerRepository.findById(customerId).orElseThrow(CMissingDataException::new);
    }

}
