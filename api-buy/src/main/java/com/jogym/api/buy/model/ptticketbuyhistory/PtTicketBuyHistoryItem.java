package com.jogym.api.buy.model.ptticketbuyhistory;

import com.jogym.api.buy.entity.PtTicketBuyHistory;
import com.jogym.common.enums.BuyStatus;
import com.jogym.common.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PtTicketBuyHistoryItem {

    private Long ptTicketBuyHistoryId;

    private String ptTicketName;

    private Integer remainCount;

    private BuyStatus buyStatus;

    private PtTicketBuyHistoryItem(Builder builder){
        this.ptTicketBuyHistoryId = builder.ptTicketBuyHistoryId;
        this.ptTicketName = builder.ptTicketName;
        this.remainCount = builder.remainCount;
        this.buyStatus = builder.buyStatus;
    }
    public static class Builder implements CommonModelBuilder<PtTicketBuyHistoryItem>{

        private final Long ptTicketBuyHistoryId;
        private final String ptTicketName;
        private final Integer remainCount;
        private final BuyStatus buyStatus;

        public Builder(PtTicketBuyHistory ptTicketBuyHistory){
            this.ptTicketBuyHistoryId = ptTicketBuyHistory.getId();
            this.ptTicketName = ptTicketBuyHistory.getPtTicket().getTicketName();
            this.remainCount = ptTicketBuyHistory.getRemainCount();
            this.buyStatus = ptTicketBuyHistory.getBuyStatus();
        }

        @Override
        public PtTicketBuyHistoryItem build() {
            return new PtTicketBuyHistoryItem(this);
        }
    }
}
