package com.jogym.api.buy.entity;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PtTicket {
    // 시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 회원등록일
    @Column(nullable = false)
    private LocalDateTime dateCreate;

    // 가맹점id
    @JoinColumn(name = "storeMemberId", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private StoreMember storeMember;

    // 직원id
    @JoinColumn(name = "trainerMemberId", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Trainer trainer;

    // PT권명
    @Column(nullable = false, length = 20)
    private String ticketName;

    // 최대횟수
    @Column(nullable = false)
    private Integer maxCount;

    // 요금
    @Column(nullable = false)
    private Double unitPrice;

    // 사용유무
    @Column(nullable = false)
    private Boolean isEnabled;


}
