package com.jogym.api.buy.entity;

import com.jogym.common.enums.BuyStatus;
import com.jogym.common.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PtTicketBuyHistory {
    // 시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 등록일시
    @Column(nullable = false)
    private LocalDateTime dateCreate;

    // 회원id
    @JoinColumn(name = "customerId", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Customer customer;

    // PT권
    @JoinColumn(name = "ptTicketId", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private PtTicket ptTicket;

    // 최대횟수
    @Column(nullable = false)
    private Integer maxCount;

    // 사용횟수
    @Column(nullable = false)
    private Integer usedCount;

    // 잔여횟수
    @Column(nullable = false)
    private Integer remainCount;

    // 상태
    @Column(nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    private BuyStatus buyStatus;

    public void putBuyStatus(BuyStatus buyStatus){
        this.buyStatus = buyStatus;
    }

    private PtTicketBuyHistory(PtTicketBuyHistoryBuilder builder) {
        this.dateCreate = builder.dateCreate;
        this.customer = builder.customer;
        this.ptTicket = builder.ptTicket;
        this. maxCount = builder.maxCount;
        this.usedCount = builder.usedCount;
        this.remainCount = builder.remainCount;
        this.buyStatus = builder.buyStatus;
    }

    public static class PtTicketBuyHistoryBuilder implements CommonModelBuilder<PtTicketBuyHistory> {

        private final LocalDateTime dateCreate;
        private final Customer customer;
        private final PtTicket ptTicket;
        private final Integer maxCount;
        private final Integer usedCount;
        private final Integer remainCount;
        private final BuyStatus buyStatus;

        public PtTicketBuyHistoryBuilder(Customer customer, PtTicket ptTicket) {
            this.dateCreate = LocalDateTime.now();
            this.customer = customer;
            this.ptTicket = ptTicket;
            this.maxCount = ptTicket.getMaxCount();
            this.usedCount = 0;
            this.remainCount = ptTicket.getMaxCount()-usedCount;
            this.buyStatus = BuyStatus.VALID;
        }

        @Override
        public PtTicketBuyHistory build() {
            return new PtTicketBuyHistory(this);
        }
    }
}
