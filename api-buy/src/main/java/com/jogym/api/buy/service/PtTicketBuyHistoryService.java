package com.jogym.api.buy.service;

import com.jogym.api.buy.entity.Customer;
import com.jogym.api.buy.entity.PtTicket;
import com.jogym.api.buy.entity.PtTicketBuyHistory;
import com.jogym.api.buy.entity.StoreMember;
import com.jogym.api.buy.model.ptticketbuyhistory.PtTicketBuyHistoryItem;
import com.jogym.api.buy.repository.PtTicketBuyHistoryRepository;
import com.jogym.common.enums.BuyStatus;
import com.jogym.common.exception.CAlreadyUsingPtTicketException;
import com.jogym.common.exception.CMissingDataException;
import com.jogym.common.response.model.ListResult;
import com.jogym.common.response.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PtTicketBuyHistoryService {
    private final PtTicketBuyHistoryRepository ptTicketBuyHistoryRepository;

    public void setPtTicketBuyHistory(Customer customer, StoreMember storeMember, PtTicket ptTicket) {
        if (!storeMember.equals(ptTicket.getStoreMember()))
            throw new CMissingDataException(); //트레이너가 해당 체육관 소속이 아니면 exception
        if (!storeMember.equals(customer.getStoreMember()))
            throw new CMissingDataException(); //회원이 해당 체육관 소속이 아니면 exception
        if (!isNewTicketOfMember(customer))
            throw new CAlreadyUsingPtTicketException(); // 회원이 동일한 체육관에서 Pt를 등록할 때 exception

        PtTicketBuyHistory ptTicketBuyHistory = new PtTicketBuyHistory.PtTicketBuyHistoryBuilder(customer, ptTicket).build();

        ptTicketBuyHistoryRepository.save(ptTicketBuyHistory);
    }

    public void putHistoryBuyStatus(StoreMember storeMember, long historyId, BuyStatus buyStatus) {
        PtTicketBuyHistory data = ptTicketBuyHistoryRepository.findByCustomer_StoreMemberAndId(storeMember, historyId).orElseThrow(CMissingDataException::new);

        data.putBuyStatus(buyStatus);

        ptTicketBuyHistoryRepository.save(data);
    }

    public ListResult<PtTicketBuyHistoryItem> getSeasonTicketBuyHistory(Customer customer, StoreMember storeMember, int page) {
        if (!customer.getStoreMember().equals(storeMember)) throw new CMissingDataException();

        Page<PtTicketBuyHistory> originData = ptTicketBuyHistoryRepository.findAllByCustomerAndPtTicket_StoreMemberOrderByIdDesc(customer, storeMember, ListConvertService.getPageable(page));

        List<PtTicketBuyHistoryItem> result = new LinkedList<>();

        originData.getContent().forEach(e -> {
            PtTicketBuyHistoryItem item = new PtTicketBuyHistoryItem.Builder(e).build();
            result.add(item);
        });
        return ListConvertService.settingResult(result, originData.getTotalElements(), originData.getTotalPages(), originData.getPageable().getPageNumber());
    }

    private boolean isNewTicketOfMember(Customer customer) {
        long dupTicket = ptTicketBuyHistoryRepository.countByCustomerAndBuyStatus(customer, BuyStatus.VALID);
        return dupTicket < 1;
    }
}
