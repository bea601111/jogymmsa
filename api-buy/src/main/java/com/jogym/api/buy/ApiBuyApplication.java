package com.jogym.api.buy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiBuyApplication {
    public static void main(String[] args) {
        SpringApplication.run(ApiBuyApplication.class, args);
    }
}
