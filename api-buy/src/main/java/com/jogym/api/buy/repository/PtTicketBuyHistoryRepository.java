package com.jogym.api.buy.repository;

import com.jogym.api.buy.entity.Customer;
import com.jogym.api.buy.entity.PtTicketBuyHistory;
import com.jogym.api.buy.entity.StoreMember;
import com.jogym.common.enums.BuyStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PtTicketBuyHistoryRepository extends JpaRepository<PtTicketBuyHistory, Long> {
    long countByCustomerAndBuyStatus(Customer customer, BuyStatus buyStatus);

    Optional<PtTicketBuyHistory> findByCustomer_StoreMemberAndId(StoreMember storeMember, Long id);

    Page<PtTicketBuyHistory> findAllByCustomerAndPtTicket_StoreMemberOrderByIdDesc(Customer customer, StoreMember storeMember, Pageable pageable);

}
