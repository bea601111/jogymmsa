package com.jogym.api.buy.model.seasonticketbuyhistory;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@ApiModel(description = "정기권 구매용 리퀘스트")
public class SeasonTicketBuyHistoryRequest {

    @ApiModelProperty(name = "회원 시퀀스 번호", required = true)
    @NotNull
    @Min(0)
    private Long customerId;

    @ApiModelProperty(name = "정기권 시퀀스 번호", required = true)
    @NotNull
    @Min(0)
    private Long seasonTicketId;

}
