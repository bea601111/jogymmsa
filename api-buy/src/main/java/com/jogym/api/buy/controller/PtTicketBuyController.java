package com.jogym.api.buy.controller;

import com.jogym.api.buy.entity.Customer;
import com.jogym.api.buy.entity.PtTicket;
import com.jogym.api.buy.entity.StoreMember;
import com.jogym.api.buy.model.ptticketbuyhistory.PtTicketBuyHistoryItem;
import com.jogym.api.buy.model.ptticketbuyhistory.PtTicketBuyHistoryRequest;
import com.jogym.api.buy.service.CustomerService;
import com.jogym.api.buy.service.PtTicketBuyHistoryService;
import com.jogym.api.buy.service.PtTicketService;
import com.jogym.common.enums.BuyStatus;
import com.jogym.common.response.model.CommonResult;
import com.jogym.common.response.model.ListResult;
import com.jogym.common.response.service.ResponseService;
import com.jogym.api.buy.service.ProfileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "PT권 구매 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/pt-ticket-buy")
public class PtTicketBuyController {
    private final PtTicketBuyHistoryService ptTicketBuyHistoryService;
    private final ProfileService profileService;
    private final CustomerService customerService;
    private final PtTicketService ptTicketService;

    @ApiOperation(value = "PT권 구매 // 가맹점") // 가맹점
    @PostMapping("/purchase")
    public CommonResult setPtTicketBuyHistory(@RequestBody @Valid PtTicketBuyHistoryRequest request){
        StoreMember storeMember = profileService.getMemberData();
        Customer customer = customerService.getData(request.getCustomerId());
        PtTicket ptTicket = ptTicketService.getPtTicketData(request.getPtTicketId());

        ptTicketBuyHistoryService.setPtTicketBuyHistory(customer,storeMember,ptTicket);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "구매내역 상태 변경 // 가맹점") // 가맹점
    @PutMapping("/buy-status/buy-history-id/{buyHistoryId}")
    public CommonResult putHistoryBuyStatus(@PathVariable long buyHistoryId, @RequestParam(value = "buy-status") BuyStatus buyStatus){
        StoreMember storeMember = profileService.getMemberData();
        ptTicketBuyHistoryService.putHistoryBuyStatus(storeMember, buyHistoryId, buyStatus);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "구매내역 리스트 // 가맹점") // 가맹점
    @GetMapping("/list/customer-id/{customerId}")
    public ListResult<PtTicketBuyHistoryItem> getSeasonTicketBuyHistory(@PathVariable long customerId, @RequestParam(value = "page",required = false, defaultValue = "1") int page){
        StoreMember storeMember = profileService.getMemberData();
        Customer customer = customerService.getData(customerId);
        return ResponseService.getListResult(ptTicketBuyHistoryService.getSeasonTicketBuyHistory(customer, storeMember,page), true);
    }
}
