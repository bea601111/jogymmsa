package com.jogym.api.buy.model.seasonticketbuyhistory;

import com.jogym.api.buy.entity.SeasonTicketBuyHistory;
import com.jogym.common.enums.BuyStatus;
import com.jogym.common.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class SeasonTicketBuyHistoryItem {

    private Long historyId;

    private String historyName;

    private LocalDate startDate;

    private LocalDate endDate;

    private Integer leftDays;

    private BuyStatus buyStatus;

    private LocalDate lastDate;

    private SeasonTicketBuyHistoryItem(Builder builder){
        this.historyId = builder.historyId;
        this.historyName = builder.historyName;
        this.startDate = builder.startDate;
        this.endDate = builder.endDate;
        this.leftDays = builder.leftDays;
        this.buyStatus = builder.buyStatus;
        this.lastDate = builder.lastDate;
    }
    public static class Builder implements CommonModelBuilder<SeasonTicketBuyHistoryItem> {

        private final Long historyId;
        private final String historyName;
        private final LocalDate startDate;
        private final LocalDate endDate;
        private final Integer leftDays;
        private final BuyStatus buyStatus;
        private final LocalDate lastDate;

        public Builder(SeasonTicketBuyHistory seasonTicketBuyHistory){
            this.historyId = seasonTicketBuyHistory.getId();
            this.historyName = seasonTicketBuyHistory.getSeasonTicket().getTicketName();
            this.startDate = seasonTicketBuyHistory.getDateStart();
            this.endDate = seasonTicketBuyHistory.getDateEnd();
            this.leftDays = (int) ChronoUnit.DAYS.between(startDate, endDate);
            this.buyStatus = seasonTicketBuyHistory.getBuyStatus();
            this.lastDate = seasonTicketBuyHistory.getDateLast();
        }

        @Override
        public SeasonTicketBuyHistoryItem build() {
            return new SeasonTicketBuyHistoryItem(this);
        }
    }
}
