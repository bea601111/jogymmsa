package com.jogym.api.buy.model.ptticketbuyhistory;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class PtTicketBuyHistoryRequest {

    @ApiModelProperty(name = "회원 시퀀스 번호", required = true)
    @NotNull
    @Min(0)
    private Long customerId;

    @ApiModelProperty(name = "PT권 시퀀스 번호", required = true)
    @NotNull
    @Min(0)
    private Long ptTicketId;

}
