package com.jogym.api.buy.controller;

import com.jogym.api.buy.entity.Customer;
import com.jogym.api.buy.entity.SeasonTicket;
import com.jogym.api.buy.entity.StoreMember;
import com.jogym.api.buy.model.seasonticketbuyhistory.SeasonTicketBuyHistoryItem;
import com.jogym.api.buy.model.seasonticketbuyhistory.SeasonTicketBuyHistoryRequest;
import com.jogym.api.buy.service.CustomerService;
import com.jogym.api.buy.service.ProfileService;
import com.jogym.api.buy.service.SeasonTicketBuyHistoryService;
import com.jogym.api.buy.service.SeasonTicketService;
import com.jogym.common.enums.BuyStatus;
import com.jogym.common.response.model.CommonResult;
import com.jogym.common.response.model.ListResult;
import com.jogym.common.response.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "정기권 구매 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/season-ticket-buy")
public class SeasonTicketBuyController {
    private final SeasonTicketBuyHistoryService seasonTicketBuyHistoryService;
    private final CustomerService customerService;
    private final SeasonTicketService seasonTicketService;
    private final ProfileService profileService;

    @ApiOperation(value = "정기권 구매 // 가맹점") // 가맹점
    @PostMapping("/purchase")
    public CommonResult setSeasonTicketBuyHistory(@RequestBody @Valid SeasonTicketBuyHistoryRequest request){
        StoreMember storeMember = profileService.getMemberData();
        Customer customer = customerService.getData(request.getCustomerId());
        SeasonTicket seasonTicket = seasonTicketService.getOriginSeasonTicket(request.getSeasonTicketId());
        seasonTicketBuyHistoryService.setSeasonTicketBuyHistory(customer, storeMember, seasonTicket);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "최근 방문일 수정 // 가맹점") // 가맹점
    @PutMapping("/date-last/buy-history-id/{buyHistoryId}")
    public CommonResult putHistoryDateLast(@PathVariable long buyHistoryId){
        StoreMember storeMember = profileService.getMemberData();
        seasonTicketBuyHistoryService.putHistoryDateLast(storeMember, buyHistoryId);

        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "구매내역 상태 변경 // 가맹점") // 가맹점
    @PutMapping("/buy-status/buy-history-id/{buyHistoryId}")
    public CommonResult putHistoryBuyStatus(@PathVariable long buyHistoryId, BuyStatus buyStatus){
        StoreMember storeMember = profileService.getMemberData();
        seasonTicketBuyHistoryService.putSeasonTicketBuyHistoryBuyStatus(storeMember, buyHistoryId, buyStatus);

        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "구매내역 리스트 // 가맹점") // 가맹점
    @GetMapping("/list/customer-id/{customerId}")
    public ListResult<SeasonTicketBuyHistoryItem> getSeasonTicketBuyHistory(@PathVariable long customerId, @RequestParam(value = "page",required = false, defaultValue = "1") int page){
        StoreMember storeMember = profileService.getMemberData();

        return ResponseService.getListResult(seasonTicketBuyHistoryService.getSeasonTicketBuyHistory(customerId, storeMember,page), true);
    }
}