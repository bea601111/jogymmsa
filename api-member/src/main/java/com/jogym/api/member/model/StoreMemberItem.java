package com.jogym.api.member.model;

import com.jogym.api.member.entity.StoreMember;
import com.jogym.common.interfaces.CommonModelBuilder;
import com.jogym.common.function.CommonFormat;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class StoreMemberItem {

    private Long id;

    private String dateCreate;

    private String username;

    private String storeName;

    private String address;

    private Boolean isEnabled;

    private StoreMemberItem(Builder builder) {
        this.id = builder.id;
        this.dateCreate = builder.dateCreate;
        this.username = builder.username;
        this.storeName = builder.storeName;
        this.address = builder.address;
        this.isEnabled = builder.isEnabled;
    }

    public static class Builder implements CommonModelBuilder<StoreMemberItem> {

        private final Long id;
        private final String dateCreate;
        private final String username;
        private final String storeName;
        private final String address;
        private final Boolean isEnabled;

        public Builder(StoreMember storeMember) {
            this.id = storeMember.getId();
            this.dateCreate = CommonFormat.convertLocalDateTimeToString(storeMember.getDateCreate());
            this.username = storeMember.getUsername();
            this.storeName = storeMember.getStoreName();
            this.address = storeMember.getAddress();
            this.isEnabled = storeMember.getIsEnabled();
        }

        @Override
        public StoreMemberItem build() {
            return new StoreMemberItem(this);
        }
    }
}
