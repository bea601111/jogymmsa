package com.jogym.api.member.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CheckIsNewUsernameResponse {

    @ApiModelProperty(notes = "아이디 중복 확인 결과 true = 성공")
    private Boolean checkResult;

}
