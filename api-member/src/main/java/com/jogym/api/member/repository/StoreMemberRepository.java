package com.jogym.api.member.repository;

import com.jogym.api.member.entity.StoreMember;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface StoreMemberRepository extends JpaRepository<StoreMember, Long> {
    Optional<StoreMember> findByUsername(String username);

    long countByUsername(String username);

    long countByBusinessNumber(String businessNumber);

    Page<StoreMember> findAllByIdGreaterThanEqualOrderByIdDesc(long id, Pageable pageable);
}
