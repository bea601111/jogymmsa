package com.jogym.api.member.service;

import com.jogym.api.member.entity.StoreMember;
import com.jogym.api.member.model.*;
import com.jogym.common.exception.*;
import com.jogym.api.member.repository.StoreMemberRepository;
import com.jogym.common.function.CommonCheck;
import com.jogym.common.response.model.ListResult;
import com.jogym.common.response.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class StoreMemberService {
    private final StoreMemberRepository storeMemberRepository;
    private final PasswordEncoder passwordEncoder;

    public void setStoreMember(StoreMemberCreateRequest createRequest) {

        if (!CommonCheck.checkUsername(createRequest.getUsername())) throw new CNotValidIdException();
        // CommonCheck 안에 있는 checkUsername 의 정보가 createRequest 안에 있는 Username 의 형식과 일치하지 않으면 던짐

        if (!createRequest.getPassword().equals(createRequest.getPasswordRe())) throw new CNotMatchPasswordException();
        // createRequest 안에 있는 password 의 정보와 createRequest 의 getPasswordRe 정보가 일치하지 않으면 던짐

        if (!isNewUsername(createRequest.getUsername()).getCheckResult()) throw new CDuplicateIdExistException();
        // 새로 만드는 아이디의 정보가 createRequest 안에 있는 Username 의 정보와 일치하면 중복됐다는 메시지와 함께 던짐

        if (!isNewBusinessNumber(createRequest.getBusinessNumber()).getCheckResult()) throw new CDuplicateBusinessNumberException();
        // 새로 들어가는 사업자 등록 번호가 createRequest 안에 BusinessNumber 의 정보와 같다면 중복됐다는 메시지와 함께 던짐

        if (!CommonCheck.checkBusinessNumber(createRequest.getBusinessNumber())) throw new CUnValidBusinessNumberException();
        // 사업자 등록번호 형식과 일치하지 않다면 던짐

        createRequest.setPassword(passwordEncoder.encode(createRequest.getPassword()));
        // createRequest 안에 Password의 정보를 암호화

        StoreMember storeMember = new StoreMember.StoreMemberBuilder(createRequest).build();
        // 원본 Entity 에 작성되어 있는 Builder 를 가져다 사용하는 부분

        storeMemberRepository.save(storeMember); // storeMember 안에 저장
    }
    public void putPassword(long storeMemberId, StoreMemberUpdatePasswordRequest request) {
        if (!request.getPassword().equals(request.getPasswordRe())) throw new CNotMatchPasswordException();
        StoreMember storeMember = storeMemberRepository.findById(storeMemberId).orElseThrow(CMissingDataException::new);
        storeMember.putPassword(passwordEncoder.encode(request.getPassword()));
        storeMemberRepository.save(storeMember);
    }

    // 가맹점 리스트
    public ListResult<StoreMemberItem> getStoreMember(int page) {
        Page<StoreMember> originList = storeMemberRepository.findAllByIdGreaterThanEqualOrderByIdDesc(1, ListConvertService.getPageable(page));
        List<StoreMemberItem> result = new LinkedList<>();

        for (StoreMember storeMember : originList.getContent()) { // 원본을 가져옴
            result.add(new StoreMemberItem.Builder(storeMember).build());
            // 미리 생성한 동아줄에 Builder 안에 있는 내용을 생성
        }

        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    // 정보 상세 보기
    public StoreMemberResponse getStoreMemberDetail(long storeMemberId) { // 특정 게시물의 상세만 봐야하기 때문에 id를 받는 과정
        StoreMember storeMember = storeMemberRepository.findById(storeMemberId).orElseThrow(CMissingDataException::new);
        return new StoreMemberResponse.Builder(storeMember).build();
    }

    // 가맹점 정보 수정
    public void putStoreMember(long storeMemberId, StoreMemberUpdateRequest request) {
        StoreMember storeMember = storeMemberRepository.findById(storeMemberId).orElseThrow(CMissingDataException::new);
        storeMember.putStoreMember(request);
        storeMemberRepository.save(storeMember);
    }

    // 가맹점 정보 삭제
    public void putStoreMemberDel(long storeMemberId) { // 특정 가맹점 정보만 지워야 하기 때문에 long 타입으로 id 를 받는다.
        StoreMember storeMember = storeMemberRepository.findById(storeMemberId).orElseThrow(CMissingDataException::new);
        // 원본 데이터에서 명령한 Id의 값과 일치하는 정보만 가져오고 없다면 없다는 문구를 건네달라고 요청한다.
        storeMember.putStoreMemberDel();
        // Repository 가 찾아온 데이터 안에 있는 put을 실행한다.
        storeMemberRepository.save(storeMember);
        // 실행한 정보를 storeMember 에 저장한다.
    }

    // 아이디 중복 확인
    public CheckIsNewUsernameResponse isNewUsername(String username) {
        long dupCount = storeMemberRepository.countByUsername(username);
        // long 타입의 dupCount 는 repository 에 입력해둔 명령어를 사용 Username 을 찾아옴
        if (!CommonCheck.checkUsername(username)) throw new CNotValidIdException();
        CheckIsNewUsernameResponse response = new CheckIsNewUsernameResponse();
        if (dupCount > 0) {response.setCheckResult(false);}
        else {response.setCheckResult(true);}
        return response;
        // 아이디의 값이 0보다 크면 거르기
    }

    // 사업자 등록번호 중복 확인
    public CheckIsNewUsernameResponse isNewBusinessNumber(String businessNumber) {
        long dupCount = storeMemberRepository.countByBusinessNumber(businessNumber);
        if (!CommonCheck.checkBusinessNumber(businessNumber)) throw new CUnValidBusinessNumberException();
        CheckIsNewUsernameResponse response = new CheckIsNewUsernameResponse();
        if (dupCount > 0) {response.setCheckResult(false);}
        else {response.setCheckResult(true);}
        return response;
        // 위 내용과 동일
    }
}
