package com.jogym.api.member.model;

import com.jogym.api.member.entity.StoreMember;
import com.jogym.common.interfaces.CommonModelBuilder;
import com.jogym.common.function.CommonFormat;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class StoreMemberResponse {
    private String dateCreate;
    private String username;
    private String name;
    private String phoneNumber;
    private String businessNumber;
    private String storeName;
    private String address;
    private Boolean isEnabled;

    private StoreMemberResponse(Builder builder) {
        this.dateCreate = builder.dateCreate;
        this.username = builder.username;
        this.name = builder.name;
        this.phoneNumber = builder.phoneNumber;
        this.businessNumber = builder.businessNumber;
        this.storeName = builder.storeName;
        this.address = builder.address;
        this.isEnabled = builder.isEnabled;
    }

    public static class Builder implements CommonModelBuilder<StoreMemberResponse> {

        private final String dateCreate;
        private final String username;
        private final String name;
        private final String phoneNumber;
        private final String businessNumber;
        private final String storeName;
        private final String address;
        private final Boolean isEnabled;

        public Builder(StoreMember storeMember) {

            this.dateCreate = CommonFormat.convertLocalDateTimeToString(storeMember.getDateCreate());
            this.username = storeMember.getUsername();
            this.name = storeMember.getName();
            this.phoneNumber = storeMember.getPhoneNumber();
            this.businessNumber = storeMember.getBusinessNumber();
            this.storeName = storeMember.getStoreName();
            this.address = storeMember.getAddress();
            this.isEnabled = storeMember.getIsEnabled();
        }
        @Override
        public StoreMemberResponse build() {
            return new StoreMemberResponse(this);
        }
    }
}
