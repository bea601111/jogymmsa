package com.jogym.api.trainer.controller;

import com.jogym.api.trainer.entity.StoreMember;
import com.jogym.common.response.model.CommonResult;
import com.jogym.common.response.model.ListResult;
import com.jogym.api.trainer.model.TrainerItem;
import com.jogym.api.trainer.model.TrainerRequest;
import com.jogym.api.trainer.model.TrainerUpdateRequest;
import com.jogym.common.response.service.ResponseService;
import com.jogym.api.trainer.service.ProfileService;
import com.jogym.api.trainer.service.TrainerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "트레이너 정보 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/trainer-member")
public class TrainerController {
    private final TrainerService trainerService;
    private final ProfileService profileService;

    @ApiOperation(value = "트레이너 등록 // 가맹점")
    @PostMapping("/data")
    public CommonResult setTrainer(@RequestBody @Valid TrainerRequest request) {
        trainerService.setTrainer(profileService.getMemberData(), request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "트레이너 리스트 // 가맹점")
    @GetMapping("/list")
    public ListResult<TrainerItem> getTrainerList(@RequestParam(value = "page", required = false, defaultValue = "1") int page) {
        StoreMember storeMember = profileService.getMemberData();
        return ResponseService.getListResult(trainerService.getTrainerList(storeMember,page), true);
    }

    @ApiOperation(value = "트레이너 상세정보 // 가맹점")
    @GetMapping("/{trainerId}")
    public CommonResult getTrainer(@PathVariable long trainerId) {
        StoreMember storeMember = profileService.getMemberData();
        return ResponseService.getSingleResult(trainerService.getTrainer(trainerId, storeMember));
    }


    @ApiOperation(value = "트레이너 정보 수정 // 가맹점")
    @PutMapping("/{trainerId}")
    public CommonResult putTrainer(@PathVariable long trainerId, @RequestBody @Valid TrainerUpdateRequest request) {
        trainerService.putTrainer(trainerId, profileService.getMemberData(), request);
        return ResponseService.getSuccessResult();
    }


    // 상태만 변경 하면 되기는 하나 상태를 변경을 해보았음.
    // service에서는 true에서 false로 바꿨으나 Controller에서는 바꾸질 못함.
    // 내가 보기에는 빌더 문제로 여겨짐..
    // 열심히 찾아 보기는 하였 으나 찾지 못하고 회원으로 넘어 가버렸음..
    @ApiOperation(value = "트레이너 삭제 // 가맹점")
    @PutMapping("/trainer-id/{trainerId}")
    public CommonResult putTrainerDelete(@PathVariable long trainerId) {
        trainerService.putTrainerDelete(trainerId, profileService.getMemberData());
        return ResponseService.getSuccessResult();
    }
}
