package com.jogym.api.use.repository;

import com.jogym.api.use.entity.PtTicketBuyHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PtTicketBuyHistoryRepository extends JpaRepository<PtTicketBuyHistory, Long> {
}
