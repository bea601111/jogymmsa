package com.jogym.api.use.service;

import com.jogym.api.use.entity.Customer;
import com.jogym.api.use.entity.SeasonTicketBuyHistory;
import com.jogym.api.use.repository.SeasonTicketBuyHistoryRepository;
import com.jogym.common.exception.CAlreadyDateCheckTodayException;
import com.jogym.common.exception.CExpireSeasonTicketDateEndException;
import com.jogym.common.exception.CMissingDataException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SeasonTicketBuyHistoryService {
    private final SeasonTicketBuyHistoryRepository seasonTicketBuyHistoryRepository;

    public SeasonTicketBuyHistory getData(long id) {
        return seasonTicketBuyHistoryRepository.findById(id).orElseThrow(CMissingDataException::new);
    }
    public void putDateLast(SeasonTicketBuyHistory seasonTicketBuyHistory){
        seasonTicketBuyHistory.putDateLastToday();
        if (LocalDate.now().isEqual(seasonTicketBuyHistory.getDateEnd()))
            seasonTicketBuyHistory.putBuyStatusComplete();
        seasonTicketBuyHistoryRepository.save(seasonTicketBuyHistory);
    }

    public void checkSeasonTicketEnd(SeasonTicketBuyHistory seasonTicketBuyHistory){
        if (LocalDate.now().isAfter(seasonTicketBuyHistory.getDateEnd())) { // 손님이 들어와서 체크하려고 할 때, 이미 날짜가 지났으면 못 들어옴
            seasonTicketBuyHistory.putBuyStatusComplete(); // 상태를 만료로 바꾸고
            seasonTicketBuyHistoryRepository.save(seasonTicketBuyHistory); // 저장 시킨다음에
            throw new CExpireSeasonTicketDateEndException(); // 만료됬다고 띄워줌
        }
    }

    public void checkToday(Customer customer){
        Optional<SeasonTicketBuyHistory> check = seasonTicketBuyHistoryRepository.findByCustomerAndDateLast(customer, LocalDate.now());
        if (check.isPresent()) throw new CAlreadyDateCheckTodayException();
    }

}

