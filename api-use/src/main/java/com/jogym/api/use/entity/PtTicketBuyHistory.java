package com.jogym.api.use.entity;

import com.jogym.common.enums.BuyStatus;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class PtTicketBuyHistory {
    // 시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 등록일시
    @Column(nullable = false)
    private LocalDateTime dateCreate;

    // 회원id
    @JoinColumn(name = "customerId", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Customer customer;

    // PT권
    @JoinColumn(name = "ptTicketId", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private PtTicket ptTicket;

    // 최대횟수
    @Column(nullable = false)
    private Integer maxCount;

    // 사용횟수
    @Column(nullable = false)
    private Integer usedCount;

    // 잔여횟수
    @Column(nullable = false)
    private Integer remainCount;

    // 상태
    @Column(nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    private BuyStatus buyStatus;

    public void expireBuyStatus(){
        this.buyStatus = BuyStatus.COMPLETE;
    }
    public void putUseCount(){
        this.usedCount++;
        this.remainCount--;
    }

}
