package com.jogym.api.use.controller;

import com.jogym.api.use.entity.Customer;
import com.jogym.api.use.entity.SeasonTicketBuyHistory;
import com.jogym.api.use.entity.StoreMember;
import com.jogym.api.use.model.seasonticketusedhistory.SeasonTicketUsedHistoryItem;
import com.jogym.api.use.service.*;
import com.jogym.common.response.model.CommonResult;
import com.jogym.common.response.model.ListResult;
import com.jogym.common.response.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@Api(tags = "정기권 이용내역 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/season-ticket-used-history")
public class SeasonTicketUsedHistoryController {
    private final SeasonTicketUsedHistoryService seasonTicketUsedHistoryService;
    private final SeasonTicketBuyHistoryService seasonTicketBuyHistoryService;
    private final CustomerService customerService;
    private final ProfileService profileService;

    @ApiOperation(value = "회원 정기권 출석 //가맹점")
    @PostMapping("/season-ticket-buy-history-id/{SeasonTicketBuyHistoryId}")
    public CommonResult setSeasonTicketUsedHistory(@PathVariable long SeasonTicketBuyHistoryId) {
        SeasonTicketBuyHistory seasonTicketBuyHistory = seasonTicketBuyHistoryService.getData(SeasonTicketBuyHistoryId);
        StoreMember storeMember = profileService.getMemberData();
        seasonTicketUsedHistoryService.isCurrentStoreMember(storeMember, seasonTicketBuyHistory); // 회원과 정기권 등록한 헬스장이 다르면 튕김
        seasonTicketBuyHistoryService.checkSeasonTicketEnd(seasonTicketBuyHistory); // 정기권이 만료 됬으면 튕김
        seasonTicketBuyHistoryService.checkToday(seasonTicketBuyHistory.getCustomer()); // 오늘 출석 했으면 튕김
        seasonTicketUsedHistoryService.setSeasonTicketUsedHistory(seasonTicketBuyHistory); // 이용내역 등록
        seasonTicketBuyHistoryService.putDateLast(seasonTicketBuyHistory); // 최근 출석일을 오늘로 수정하고 구매내역 완료로 변경

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "정기권 이용내역 리스트 //가맹점")
    @GetMapping("/list/customer-id/{customerId}")
    public ListResult<SeasonTicketUsedHistoryItem> getData(@RequestParam(value = "page", required = false, defaultValue = "1")int page, @PathVariable long customerId) {
        Customer customer = customerService.getData(customerId);
        StoreMember storeMember = profileService.getMemberData();
        return ResponseService.getListResult(seasonTicketUsedHistoryService.getData(page, customer, storeMember), true);
    }
}
