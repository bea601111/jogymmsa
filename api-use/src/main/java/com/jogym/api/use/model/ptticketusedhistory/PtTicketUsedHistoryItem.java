package com.jogym.api.use.model.ptticketusedhistory;

import com.jogym.api.use.entity.PtTicketUsedHistory;
import com.jogym.common.function.CommonFormat;
import com.jogym.common.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PtTicketUsedHistoryItem {

    private String dateCreate;
    private String ptName;
    private String trainerName;
    private Integer maxCount;
    private Integer usedCount;
    private Integer remainCount;

    private PtTicketUsedHistoryItem(Builder builder){
        this.dateCreate = builder.dateCreate;
        this.ptName = builder.ptName;
        this.trainerName = builder.trainerName;
        this.maxCount = builder.maxCount;
        this.usedCount = builder.usedCount;
        this.remainCount = builder.remainCount;
    }
    public static class Builder implements CommonModelBuilder<PtTicketUsedHistoryItem>{

        private final String dateCreate;
        private final String ptName;
        private final String trainerName;
        private final Integer maxCount;
        private final Integer usedCount;
        private final Integer remainCount;

        public Builder(PtTicketUsedHistory ptTicketUsedHistory){
            this.dateCreate = CommonFormat.convertLocalDateTimeToString(ptTicketUsedHistory.getDateCreate());
            this.ptName = ptTicketUsedHistory.getPtTicketBuyHistory().getPtTicket().getTicketName();
            this.trainerName = ptTicketUsedHistory.getPtTicketBuyHistory().getPtTicket().getTrainer().getName();
            this.maxCount = ptTicketUsedHistory.getMaxCount();
            this.usedCount = ptTicketUsedHistory.getUsedCount();
            this.remainCount = ptTicketUsedHistory.getRemainCount();
        }

        @Override
        public PtTicketUsedHistoryItem build() {
            return new PtTicketUsedHistoryItem(this);
        }
    }
}
