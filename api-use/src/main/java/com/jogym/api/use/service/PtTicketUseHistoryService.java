package com.jogym.api.use.service;

import com.jogym.api.use.entity.*;
import com.jogym.api.use.model.ptticketusedhistory.PtTicketUsedHistoryItem;
import com.jogym.api.use.repository.PtTicketUsedHistoryRepository;
import com.jogym.common.exception.CMissingDataException;
import com.jogym.common.response.model.ListResult;
import com.jogym.common.response.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PtTicketUseHistoryService {
    private final PtTicketUsedHistoryRepository ptTicketUsedHistoryRepository;

    public void setPtTicketUsedHistory(PtTicketBuyHistory ptTicketBuyHistory){
        PtTicketUsedHistory ptTicketUsedHistory = new PtTicketUsedHistory.PtTicketUsedHistoryBuilder(ptTicketBuyHistory).build();

        ptTicketUsedHistoryRepository.save(ptTicketUsedHistory);
    }

    public ListResult<PtTicketUsedHistoryItem> getPtTicketUsedHistoryList(StoreMember storeMember, Customer customer, int page){
        if (!customer.getStoreMember().equals(storeMember)) throw new CMissingDataException();
        Page<PtTicketUsedHistory> originList = ptTicketUsedHistoryRepository.findAllByPtTicketBuyHistory_Customer_StoreMemberAndPtTicketBuyHistory_Customer(storeMember, customer, ListConvertService.getPageable(page));

        List<PtTicketUsedHistoryItem> result = new LinkedList<>();

        originList.getContent().forEach(e->{
            PtTicketUsedHistoryItem item = new PtTicketUsedHistoryItem.Builder(e).build();

            result.add(item);
        });
        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    public void checkIsCurrentStoreMember(StoreMember storeMember, PtTicketBuyHistory ptTicketBuyHistory){
        if (!storeMember.equals(ptTicketBuyHistory.getPtTicket().getStoreMember())) throw new CMissingDataException();
    }
}
