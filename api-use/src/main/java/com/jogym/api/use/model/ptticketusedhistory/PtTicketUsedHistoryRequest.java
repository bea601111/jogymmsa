package com.jogym.api.use.model.ptticketusedhistory;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class PtTicketUsedHistoryRequest {
    @NotNull
    @Min(1)
    @Max(1000)
    @ApiModelProperty(notes = "최대횟수(1~1000)",  required = true)
    private Integer maxCount;

    @NotNull
    @Min(1)
    @Max(1000)
    @ApiModelProperty(notes = "사용횟수(1~1000)",  required = true)
    private Integer usedCount;

    @NotNull
    @Min(1)
    @Max(1000)
    @ApiModelProperty(notes = "잔여횟수(1~1000)",  required = true)
    private Integer remainCount;
}
