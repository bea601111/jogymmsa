package com.jogym.api.use.controller;

import com.jogym.api.use.entity.Customer;
import com.jogym.api.use.entity.PtTicketBuyHistory;
import com.jogym.api.use.entity.StoreMember;
import com.jogym.api.use.model.ptticketusedhistory.PtTicketUsedHistoryItem;
import com.jogym.api.use.service.CustomerService;
import com.jogym.api.use.service.ProfileService;
import com.jogym.api.use.service.PtTicketBuyHistoryService;
import com.jogym.api.use.service.PtTicketUseHistoryService;
import com.jogym.common.response.model.CommonResult;
import com.jogym.common.response.model.ListResult;
import com.jogym.common.response.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@Api(tags = "PT권 이용내역 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/pt-ticket-use-history")
public class PtTicketUsedHistoryController {
    private final PtTicketUseHistoryService ptTicketUseHistoryService;
    private final PtTicketBuyHistoryService ptTicketBuyHistoryService;
    private final CustomerService customerService;
    private final ProfileService profileService;

    @ApiOperation(value = "PT권 사용 // 가맹점")
    @PostMapping("/data/pt-buy-id/{ptTicketBuyHistoryId}")
    public CommonResult setPtTicketUseHistory(@PathVariable long ptTicketBuyHistoryId){
        PtTicketBuyHistory ptTicketBuyHistory = ptTicketBuyHistoryService.getData(ptTicketBuyHistoryId);
        StoreMember storeMember = profileService.getMemberData();
        ptTicketUseHistoryService.checkIsCurrentStoreMember(storeMember, ptTicketBuyHistory); // 헬스장과 pt권이 같은 곳의 것인지 확인 먼저하고
        ptTicketBuyHistoryService.isExpirePtTicket(ptTicketBuyHistory); // pt권 사용횟수를 전부 소진하면 튕김. pt 횟수가 남았으면 사용 횟수 + 1, 잔여 횟수 - 1
        ptTicketUseHistoryService.setPtTicketUsedHistory(ptTicketBuyHistory);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "손님 PT권 사용 내역 // 가맹점")
    @GetMapping("/list/customer-id/{customerId}")
    public ListResult<PtTicketUsedHistoryItem> getPtTicketUseHistoryList(@PathVariable long customerId, @RequestParam(value = "page",required = false, defaultValue = "1") int page){
        StoreMember storeMember = profileService.getMemberData();
        Customer customer = customerService.getData(customerId);
        return ResponseService.getListResult(ptTicketUseHistoryService.getPtTicketUsedHistoryList(storeMember, customer, page), true);
    }
}
