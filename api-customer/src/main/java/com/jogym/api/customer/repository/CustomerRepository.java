package com.jogym.api.customer.repository;

import com.jogym.api.customer.entity.Customer;
import com.jogym.api.customer.entity.StoreMember;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
    Page<Customer> findAllByStoreMemberAndIsEnabledOrderByDateCreateDesc(StoreMember storeMember, Boolean isEnable, Pageable pageable);
    Optional<Customer> findByIdAndStoreMember(Long customerId, StoreMember storeMember);
}
