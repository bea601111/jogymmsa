package com.jogym.api.customer.model;

import com.jogym.common.enums.Gender;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class CustomerUpdateRequest {
    @ApiModelProperty(notes = "이름", required = true)
    @NotNull
    private String name;

    @ApiModelProperty(notes = "연락처", required = true)
    @NotNull
    private String phoneNumber;

    @ApiModelProperty(notes = "주소", required = true)
    @NotNull
    private String address;

    @ApiModelProperty(notes = "성별", required = true)
    @NotNull
    private Gender gender;

    @ApiModelProperty(notes = "생년월일", required = true)
    @NotNull
    private LocalDate dateBirth;

    @ApiModelProperty(notes = "비고", required = true)
    private String memo;
}
