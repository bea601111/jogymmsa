package com.jogym.api.customer.model;

import com.jogym.api.customer.entity.Customer;
import com.jogym.common.function.CommonFormat;
import com.jogym.common.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CustomerItem {
    @ApiModelProperty(notes = "시퀀스", required = true)
    @NotNull
    private Long id;

    @ApiModelProperty(notes = "등록일", required = true)
    @NotNull
    private String dateCreate;

    @ApiModelProperty(notes = "이름", required = true)
    @NotNull
    private String name;

    @ApiModelProperty(notes = "연락처", required = true)
    @NotNull
    private String phoneNumber;

    private CustomerItem(Builder builder) {
        this.id = builder.id;
        this.dateCreate = builder.dateCrate;
        this.name = builder.name;
        this.phoneNumber = builder.phoneNumber;
    }

    public static class Builder implements CommonModelBuilder<CustomerItem> {
        private final Long id;
        private final String dateCrate;

        private final String name;
        private final String phoneNumber;

        public Builder(Customer customer) {
            this.id = customer.getId();
            this.dateCrate = CommonFormat.convertLocalDateTimeToString(customer.getDateCreate());
            this.name = customer.getName();
            this.phoneNumber = customer.getPhoneNumber();
        }

        @Override
        public CustomerItem build() {
            return new CustomerItem(this);
        }
    }
}
